﻿using System.Collections;

namespace _04_Array_ArrayList_Dictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Mengimplementasikan Array
            string[] courses = new string[3];
            courses[0] = "Pemrograman Dasar";
            courses[1] = "Pemrograman Lanjut";
            courses[2] = "Algoritma dan Struktur Data";

            Console.WriteLine("Menampilkan array courses:");
            foreach(string c in courses)
            {
                Console.Write(c + ", ");
            }

            // Mengimplementasikan ArrayList
            ArrayList students = new ArrayList();
            students.Add("Syaugi");
            students.Add(16);
            students.Add(true);
            students.Add("Claudia");

            Console.WriteLine("\nMenampilkan ArrayList students:");
            foreach (object obj in students)
            {
                Console.Write(obj.ToString() + ", ");
            }

            // Mengimplementasikan Dictionary
            Dictionary<string, int> Human = new Dictionary<string, int>();
            Human.Add("Sulistia", 21);
            Human.Add("Hafid", 15);
            Human.Add("Azim", 6);

            Console.WriteLine("\nMenampilkan Dictionary Human:");
            foreach (KeyValuePair<string, int> human in Human)
            {
                Console.WriteLine(human.Key + " berumur " + human.Value + " tahun.");
            }

            // Mengimplementasikan Dictionary dengan tipe data key: string, value: Student
            Dictionary<string, Student> Students = new Dictionary<string, Student>();
            Student s1 = new Student("s1", "Ali");
            Student s2 = new Student("s2", "Syaugi");
            Student s3 = new Student("s3", "Alkaf");

            Students.Add(s1.studentId, s1);
            Students.Add(s2.studentId, s2);
            Students.Add(s3.studentId, s3);

            foreach (KeyValuePair<string, Student> s in Students)
            {
                Console.WriteLine(s.Key + "  >>>>>  " + s.Value.studentName);
                Console.WriteLine(s.Value.ToString());
            }
        }
    }

    public class Student
    {
        public string studentId;
        public readonly string studentName;

        public Student(string studentId, string studentName)
        {
            this.studentId = studentId;
            this.studentName = studentName;
        }
    }
}